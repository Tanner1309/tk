import mysql.connector
from tkinter import ttk
from tkinter import *
class Product:

    def __init__(self, window):
        self.wind = window
        self.wind.title("Productos")
       
        #Creacion de frmae
        frame=LabelFrame(self.wind,text='Registrar nuevo producto')
        frame.grid(row=0,column=0,columnspan=3,pady=20)
       
        #Input de nombre y precio
        Label(frame,text="Nombre:").grid(row=1,column=0)
        self.name=Entry(frame)
        self.name.grid(row=1, column=1)
        Label(frame,text="Precio:").grid(row=2,column=0)
        self.price=Entry(frame)
        self.price.grid(row=2, column=1)
       
        #Boton
        ttk.Button(frame, text="Guardar datos", command = self.add_product).grid(row=3,columnspan=2, sticky=W+E)
        #Tabla de datos
        self.tree = ttk.Treeview(columns=('Name','Price'))
        self.tree.grid(row=4, column=0, columnspan=3)
        self.tree.heading('#0',text='ID', anchor=CENTER)
        self.tree.heading('Name',text='Nombre', anchor=CENTER)
        self.tree.heading('Price',text='Precio', anchor=CENTER)
        self.get_products()     

    def  run_comquery(self, query):
        dato = { 
        'user':'root',
        'password':'Ulises13',
        'database':'Pruebas',
        'host':'127.0.0.1' }
        conexion = mysql.connector.connect(**dato)
        cursor = conexion.cursor()
        cursor.execute(query)
        conexion.commit()


    def get_products(self):
        records= self.tree.get_children()
        for element in records:
            self.tree.delete(element)
        dato = { 
        'user':'root',
        'password':'Ulises13',
        'database':'Pruebas',
        'host':'127.0.0.1' }
        conexion = mysql.connector.connect(**dato)
        cursor = conexion.cursor()
        valores="Select * From Productos"
        cursor.execute(valores)
        row=cursor.fetchone()
        while row is not None:
            self.tree.insert('',0,text=row[0],values=(row[1],row[2]))
            row=cursor.fetchone()
    
    def add_product(self):
        if len(self.name.get()) !=0 and len(self.price.get()) !=0:
            name=(self.name.get())
            price=(self.price.get())
            query=f"INSERT INTO Productos(Nombre,Precio) VALUES ('{name}','{price}');"
            self.run_comquery(query)
            self.get_products()
            self.name.delete(0,len(self.name.get()))
            self.price.delete(0,len(self.price.get()))
                   

if __name__ == '__main__':
  window = Tk()
  app = Product(window)
  window.mainloop()
